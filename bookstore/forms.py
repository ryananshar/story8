from django import forms
from . import models

class SearchBar(forms.Form):
	attrs = {
			'id':'query-search', 
			'placeholder':'Title or Author',
			'type':'query'
			}
	query = forms.CharField(label='', max_length=200, 
							widget=forms.TextInput(attrs=attrs))