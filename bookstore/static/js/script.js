function bookSearch(query){
	event.preventDefault();
	var search = document.getElementById('query-search').value
	console.log(search);

	if (query == "Harry"){
		$.ajax({
			type: "GET",
			url: "/bookjson",
			data:{
			            'query': query
			        },
			dataType: 'json',
			success: function(json) {
				$('#search-content').empty();
	            $('#search-content').append('<thead class="thead-dark"><tr><th scope = "col">#</th><th scope = "col">Gambar</th><th scope = "col">Judul</th><th scope = "col">Deskripsi</th><th scope = "col">Authors</th></tr></thead>')

				var books = json.items
				for (i = 0; i < books.length; i++) {
					var id_num = i+1;
					var title = books[i].volumeInfo.title;
					if(title == undefined){
	                    title = "---No title---";
	                }
	                var foto = books[i].volumeInfo.imageLinks.thumbnail;
	                var img= '<img src=foto>';
					if(foto == undefined){
	                    foto = "---No image found---";
	                }
					var desc = books[i].volumeInfo.description;
					if(desc == undefined){
	                    desc = "---No description found---";
	                }
					var author = books[i].volumeInfo.authors;
					if(author == undefined){
	                    author = "---No authors found---";
	                }
	                $('#search-content').append('<tbody><tr><th>' + id_num + '</th><td><img src='+ foto +'></td><th>' + title + '</th><th><p>' 
	                	+ desc + '</p></th><th>' + author + '</th></tr></tbody>');
					
				}
			}
		})
		return false;
	} else {
		$.ajax({
		type: "GET",
		url: "/bookjson",
		data:{
		            'query': search
		        },
		dataType: 'json',
		success: function(json) {
			$('#search-content').empty();
            $('#search-content').append('<thead class="thead-dark"><tr><th scope = "col">#</th><th scope = "col">Gambar</th><th scope = "col">Judul</th><th scope = "col">Deskripsi</th><th scope = "col">Authors</th></tr></thead>')
			var books = json.items
				for (i = 0; i < books.length; i++) {
					var id_num = i+1;
					var title = books[i].volumeInfo.title;
					if(title == undefined){
	                    title = "---No title---";
	                }
	                var foto = books[i].volumeInfo.imageLinks.thumbnail;
	                var img= '<img src=foto>';
					if(foto == undefined){
	                    foto = "---No image found---";
	                }
					var desc = books[i].volumeInfo.description;
					if(desc == undefined){
	                    desc = "---No description found---";
	                }
					var author = books[i].volumeInfo.authors;
					if(author == undefined){
	                    author = "---No authors found---";
	                }
	                $('#search-content').append('<tbody><tr><th>' + id_num + '</th><td><img src='+ foto +'></td><th>' + title + '</th><th><p>' 
	                	+ desc + '</p></th><th>' + author + '</th></tr></tbody>');
			}
		}
	})
	return false;
	}
	
}
// document.getElementById('searchButton').addEventListener('click', bookSearch, false)
$(document).on("submit", "#search-form", function(event){
    event.preventDefault();
    var query = $('#query-form').val();
    bookSearch(query);
});

$(document).ready(function(){
    var query = "Harry";
    bookSearch(query);
});