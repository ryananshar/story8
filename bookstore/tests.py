from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class UnitTest(TestCase):
	def test_url_exists(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_url_that_does_not_exist(self):
		response = Client().get('/nothing/')
		self.assertEqual(response.status_code, 404)

	def test_template_exists(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_landing_page_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_form_is_inside_template(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('<form', content)

	def test_button_is_inside_template(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('<button', content)
		self.assertIn('Search', content)
