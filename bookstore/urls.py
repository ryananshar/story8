from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='home'),
    path('bookjson/', views.bookjson, name='bookjson'),
]