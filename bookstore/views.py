from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .forms import SearchBar
#pip install requests
import requests

# Create your views here.
def index(request):
	context = {
				'searchbar' : SearchBar
	}
	return render(request, 'index.html', context)

def bookjson(request):
    query = request.GET.get('query')
    url = "https://www.googleapis.com/books/v1/volumes?q=" + query
    return JsonResponse(requests.get(url).json())
